﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnEvent
{
    class Program
    {
      

        

        static void Pidu(string põhjus)
        {
            Console.WriteLine($"Saab pidu: {põhjus}");

        }

        static void Mine(string põhjus)
        {
            Console.WriteLine($"Mine {põhjus}");
        }

        static void Main(string[] args)
        {
            // ma nüüd kukkusin programmeerima

            Inimene henn = new Inimene { Nimi = "Henn", Vanus = 45 };


            henn.Juubel += Pidu; // event händler valmis meetod
            henn.Juubel += (x) => Console.WriteLine($"ohoo {x}");
                        // event hänadler lambdana
            henn.PensioniIga += Mine;
            henn.PensioniIga += Pidu;
            

            while(henn.Vanus < 70)
            {
                henn.Vanus++;
                Console.WriteLine("Hennu vanus on {0}", henn.Vanus);
            }

            henn.Vanus = 65; // uuesti pensionile ei saadeta (enne oli vanus > 65)
            henn.Vanus = 50; // pidu saab
            henn.Vanus = 65; // saadetakse pensionile (enne oli vanus < 65)



        }
    }

    // andmetüübi kirjeldus muutujale, mille väärtuseks võib olla
    // meetod (void) parameetriga string
    public delegate void MiskiMeetod(string x);
    // reaalselt on sellline valmis kirjeldatud genericuna
    // Action<string>

    class Inimene
    {
        // see on üsna tavaline property - pelgalt get & set
        public string Nimi { get; set; }
        int _Vanus;

        // see on selline 'kontrollitud' property get on tavaline
        // aga set-i saaks juba rikkalikumaks teha
        public int Vanus
        {
            get => _Vanus;
            set 
            {
                
                if (value > 64 && _Vanus <= 64)
                {
                    // siis teeme midagi
                    if (PensioniIga != null) PensioniIga("pensionile");
                }

                if (value % 25 == 0) Juubel?.Invoke($"Henn sai {value}");
                _Vanus = value;
            }
        }

        public Action<string> PensioniIga; // siin on kirjeldatud event
        public Action<string> Juubel;

        
        
       

    }
}
