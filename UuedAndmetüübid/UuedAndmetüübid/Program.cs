﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UuedAndmetüübid
{
    class Program
    {
        static void Main(string[] args)
        {
            // õpime uue andmetüübi (perekonna) nullable
            // seda läheb sageli vaja, eriti kui tegu andmebaasidega
            // või andmetega, mis tulevad mujalt
            #region esimene osa
            //int a = 7;

            //int? na = null;

            //a = na ?? 0;

            //string nimi = null;
            //Console.WriteLine(nimi?.ToUpper()??"puudub");
            //string[] nimed = { "Henn", "Ants", null, "Peeter" };
            //foreach(var x in nimed)
            //    Console.WriteLine(x?.ToUpper()??"me ei tea");

            //Inimene henn = new Inimene
            //{
            //    Nimi = "Henn"
            //};
            //henn.Isa = new Inimene
            //{
            //    Nimi = "Henn"
            //};
            //henn.Isa.Isa = new Inimene { Nimi = "Jaan" };
            //henn.Ema = new Inimene
            //{
            //    Nimi = "Leili",
            //    Isa = new Inimene { Nimi = "Kusta" }
            //};

            //Console.WriteLine("\nHennu vanaisa oli:\n");
            //Console.WriteLine(henn.Isa.Isa.Nimi);
            //Console.WriteLine(henn.Ema.Isa?.Nimi??"teda me ei tea"); 
            #endregion

            // teine uus asi on IEnumerable asjandus
            // aga enne natuke, mis asi on generic

            int a = 3;
            int b = 4;
            Console.WriteLine($"enne: a={a} ja b={b}");
            Vaheta(ref a, ref b);
            Console.WriteLine($"pärast: a={a} ja b={b}");

            string üks = "Henn";
            string teine = "Ants";
            Console.WriteLine($"enne: üks={üks} ja teine={teine}");
            Vaheta<string>(ref üks, ref teine);
            Console.WriteLine($"pärast: üks={üks} ja teine={teine}");

            Paar<int> arvud = new Paar<int> { Parem = 7, Vasak = 8 };

            int[] arvud1 = { 1, 2, 3, 4, 5 };
            List<int> arvud2 = new List<int> { 1, 2, 3, 4, 5 };
            Console.WriteLine(Liida(arvud1));
            Console.WriteLine(Liida(arvud2));

            Console.WriteLine(
            arvud1.Where(x => x > 10).DefaultIfEmpty().Average(x => x)
            );

        }

        static int Liida(IEnumerable<int> m)
        {
            int sum = 0;
            foreach (var x in m) sum += x;
            return sum;
        }

        static void Vaheta<T>(ref T x, ref T y)
        {
            T ajuti = x;
            x = y;
            y = ajuti;
        }




    }

    class Paar<T>
    {
        public T Vasak;
        public T Parem;
    }

    class Inimene
    {
        public string Nimi { get; set; } = "";
        public Inimene Isa { get; set; }
        public Inimene Ema { get; set; }

    }
}
