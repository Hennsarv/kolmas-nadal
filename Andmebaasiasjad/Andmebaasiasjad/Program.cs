﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Andmebaasiasjad
{
    class Program
    {
        static void Main(string[] args)
        {
            //SqlConnection conn = new SqlConnection("Data Source=valiit.database.windows.net;Initial Catalog=northwind;Persist Security Info=True;User ID=student;Password=Pa$$w0rd");
            //conn.Open();
            //SqlCommand comm = new SqlCommand("select * from employees", conn);
            //var R = comm.ExecuteReader();
            //while(R.Read())
            //    Console.WriteLine($"{R["FirstName"]} {R["LastName"]}" );
            //conn.Close();

            northwindEntities db = new northwindEntities();
            foreach(var x in db.Products)
                Console.WriteLine(x.ProductName);

            var q = from x in db.Products
                    where x.UnitPrice > 20
                    orderby x.UnitPrice descending
                    select new { x.ProductName, x.UnitPrice };

            Console.WriteLine(q);

            foreach (var x in q) Console.WriteLine(x);

            var q2 = from c in db.Categories
                     select new { c.CategoryName, Arv = c.Products.Count };
            Console.WriteLine(q2);
            foreach (var x in q2) Console.WriteLine(x);




        }
    }
}
