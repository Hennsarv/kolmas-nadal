﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Torutööd
{
    static class Program
    {
        static IEnumerable<int> Paaris(this IEnumerable<int> arvud)
        {
            foreach (var x in arvud)
                if (x % 2 == 0)
                {
                    yield return x; // yield return        
                    Console.WriteLine($"annan tagasi {x}");
                }
                else
                {
                    Console.WriteLine($"{x} ei ole paaris, seda ei anna");
                }
            Console.WriteLine("otsas - täna rohkem ei saa");
        }

        static IEnumerable<int> Paaritu(this IEnumerable<int> arvud)
        {
            foreach (var x in arvud) if (x % 2 != 0) yield return x;
        }

        static IEnumerable<int> KaksArvu()
        {
            yield return 7;
            yield return 3;
        }

        //static bool KasPaaris(int arv) => arv % 2 == 0;
        //static bool KasJagubKolmega(int arv) => arv % 3 == 0;

        //static IEnumerable<int> 
        //    Where(this IEnumerable<int> arvud, Func<int,bool> f)
        //{
        //    foreach (var x in arvud) if (f(x)) yield return x;
        //}

        static void ForEach2<T>(this IEnumerable<T> m, Action<T> a)
        {
            foreach (var x in m) a(x);
        }

        static void Main(string[] args)
        {
            // kui sa tahad kohvi, siis viimane aeg
            // kohe paneme ukse lukku, et sa kaduda ei saaks

            int[] arvud = { 2, 5, 3, 1, 7, 9, 6, 4, 8 };

            foreach (var x in arvud.Where(y => y % 2 == 0)) Console.WriteLine(x);

            Inimene[] inimesed =
            {
                new Inimene{Nimi = "Henn", Sugu = Sugu.Mees, Vanus = 63},
                new Inimene{Nimi = "Ants", Sugu = Sugu.Mees, Vanus = 40},
                new Inimene{Nimi = "Pille", Sugu = Sugu.Naine, Vanus = 22},
                new Inimene{Nimi = "Kalle", Sugu = Sugu.Mees, Vanus = 28},
                new Inimene{Nimi = "Malle", Sugu = Sugu.Naine, Vanus = 32},
            };

            foreach(var x in inimesed
                .Where( i => i.Sugu == Sugu.Mees)
                .OrderBy( i => i.Nimi)
                )
                Console.WriteLine(x.Nimi);

            Console.WriteLine(inimesed
                                .Where(x => x.Sugu == Sugu.Naine)
                                .Count()
                            );

            inimesed
                .GroupBy(i => i.Sugu)
                .Select(i => new { sugu = i.Key, arv = i.Count() }
                )

                //.ToList()
                .ForEach2(x => Console.WriteLine(x));

            arvud.ForEach2(x => Console.WriteLine(x * x));

            Console.WriteLine(inimesed.Average(x => x.Vanus));
            inimesed.GroupBy(i => i.Sugu)
                .Select(i => new { sugu = i.Key, keskmine = i.Average(j => j.Vanus) })
                .ForEach2(x => Console.WriteLine(x));

            Dictionary<string, Inimene> jaotus = inimesed.ToDictionary(x => x.Nimi, x => x);
            Console.WriteLine(jaotus["Henn"].Vanus);

            // kuidas tagada, et average ei anna viga
            Console.WriteLine(
                inimesed.Where(x => x.Vanus > 100) // selliseid inimesi ei ole
                .DefaultIfEmpty() // see paneb tühja torusse (t[0]) null objekti - NB! aga õiget tüüpi)
                .Average(x => x?.Vanus??0) // nüüd arvutame keskmise
                            // kui x == null siis x?.Vanus on null (int? tüüpi)
                            // ??0 asendab kandilise nulli ümmarguse 0-ga
                );


        }
    }
    enum Sugu { Naine, Mees}
    class Inimene
    {
        public string Nimi;
        public Sugu Sugu;
        public int Vanus;
    }
}
