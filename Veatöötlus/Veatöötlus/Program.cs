﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Veatöötlus
{
    class Program
    {
        static void Main(string[] args)
        {

            while (true)
            {
                try
                {
                    Console.Write("mitu õlut meil on: ");
                    int a = int.Parse(Console.ReadLine());
                    Console.Write("palju joojaid on: ");
                    int b = int.Parse(Console.ReadLine());
                    if (b > a) throw new Exception("kõigile ei jagu niiehknii");
                    Console.WriteLine($"õlut jagub igale {a / b}");
                    break;
                }
                catch (DivideByZeroException e)
                {
                    Console.WriteLine("null pole nummer");
                }
                catch (FormatException e)
                {
                    Console.WriteLine("toksi numbreid");
                }
                catch (Exception e)
                {
                    Console.Write("midagi läks nihu: ");
                    Console.WriteLine(e.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("aga pea valutab ikkagi");
                }
            }
        }
    }
}
