﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Toruülesanne
{
    

    class Program
    {
        static void Main(string[] args)
        {
            // see lause loeb faili sisse ja saab stringimassiivi
            var hindedTxt = File.ReadAllLines(@"..\..\hinded.txt");

            var hinded =
                // esimene samm - muudame stringid selles massiivis ise stringimassiiviks
                // iga rida (x) splitime (saame stringimassiivi)
                //              seejärel trimmime kõik selle elemendid
                //              seejärel muudame ta uuesti massiiviks
            hindedTxt
            .Select(x => x.Split(',').Select(y => y.Trim()).ToArray())

            // nüüd teisendame saadud massiivid paarideks Aine ja Hinne
            // kus Aine on kolmas(2) ja hinne neljas(3) element
            //.Select(x => new { ÕpilaseIK = x[1], Aine = x[2], Hinne = int.Parse(x[3]) })
            .Select(x => new { ÕpilaseIK = x[1], Aine = x[2], Hinne = int.Parse(x[3]) })

            .ToList(); // lõpuks muudame tulemuse Listiks
            // NB! rida 17 kuni rida 28 on kõik 1 avaldis-tehe
            foreach (var x in hinded) Console.WriteLine(   x );

            // nüüd proovime saadud tulemusest leida keskmine hinne
            // ja seejärel keskmine hinne ainete kaupa

            // mina nüüd proovin midagi muud teha

            var q0 = hinded.GroupBy(x => x.Aine)
                    .Select(x => new { Aine = x.Key, Keskmine = x.Average(y => y.Hinne) })
                    .OrderBy(x => x.Keskmine)
                    ;
            
            // see on vähe jõuga vägistatud orderby
            var q1 = from x in hinded
                     group x by x.Aine into g
                     orderby g.Average(h => h.Hinne)
                     select new { Aine = g.Key, Keskmine = g.Average(h => h.Hinne) }
                     ;

            // see on sama tehtud 'alampäringuna'
            var q4 = from y in
                         from x in hinded
                         group x by x.Aine into g
                         select new { Aine = g.Key, Keskmine = g.Average(h => h.Hinne) }
                     orderby y.Keskmine descending
                     select y;

            var q2 = hinded.GroupBy(x => x.ÕpilaseIK)
                    .Select(x => new { ÕpilaseIK = x.Key, Keskmine = x.Average(y => y.Hinne) })
                    .OrderByDescending(x => x.Keskmine)
                    ;

            Console.WriteLine("\nq0");
            foreach (var x in q0) Console.WriteLine(x);
            Console.WriteLine("\nq1");
            foreach (var x in q1) Console.WriteLine(x);
            Console.WriteLine("\nq4");
            foreach (var x in q4) Console.WriteLine(x);
            Console.WriteLine("\nq2");
            foreach (var x in q2) Console.WriteLine(x);

        }
    }
}
