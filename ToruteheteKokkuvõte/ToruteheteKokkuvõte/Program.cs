﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToruteheteKokkuvõte
{
    class Program
    {
        static void Main(string[] args)
        {
            // esiteks paar sõna lambdadest ja delegaatidest
            // delegaat on andmetüüp, mille väärtuseks on funktsioon
            // süsteemis on kaks valmisehitatud generic delegaati
            // Func<> - vastav funktsioonile
            // ja Action<> - vastab meetodile

            Func<int, int> funktsioon; // muutuja, mille väärtuseks on funktsioon
                                       // parameetriga int ja vastusega int

            funktsioon = x => x * x;   // muutja väärtustamine lambdaga
            Console.WriteLine(funktsioon(4));   // funktsioonmuutja kasutamine

            Action<int> tegevus;    // muutuja, mille väärtuseks võib olla 
                                    // meetod (void) parameetriga int

            tegevus = x => Console.WriteLine(x); // tegevuse väärtustamine lambdaga

            tegevus(4); // trükib konsoolile 4
            tegevus(funktsioon(4)); // trükib konsoolile 16

            // delegaatide peamine võlu on, et neid saab dünaamiliselt muuta ja
            // anda parameetrina ette funktsioonidele

            int[] arvud = { 1, 2, 3, 4, 5 };

            arvud.Where(x => x > 3).Select(funktsioon).ToList().ForEach(tegevus);
            // konsoolile trükitakse 16 ja 25

            // enamasti paigutatakse funktsioon-parameeter otse lambdaavaldisena 

            arvud.Select(x => x * x).ToList().ForEach(x => Console.WriteLine(x));

            // Jätkame nüüd extensionitega ja LINQ-avaldistega

            foreach (var x in arvud.Where(x => x > 3).Select(x => x)) Console.WriteLine(x);
            // trükib kõik arvud, mis suuremad kui 3

            foreach (var x in from x in arvud where x > 3 select x) Console.WriteLine(x);
            // teeb täpselt sedasama

            // väike selgitus
            // 1. variandis on tegemist extensioniga (Where), 
            //      millele parameetriks on funktsioon "kas x > 3"

            // 2. variandis on sama kirja pandud LINQ avaldisega
            //      algab fraasiga from ja lõpeb fraasiga select

            //      täpselt samaväärsed oleks need laused, 1. variant
            //      oleks kirjutatud arvud.Where(x => x > 3).Select(x => x)
            //      extensionina kirjutades võib viimase - tühifunktsiooni - ära jätta
            //      LINQ avaldisena peab see olemas olema

            // Allpool nüüd järjest kõik peamised extensionid 
            //      koos LINQ parallelkujuga (kui see on)
            //      ning koos tavalise kasutusviisiga

            // Kõne all on Extensionid, mis toimivad IEnumerable<T> andmetüüpidega
            //      IEnumerable<T> objektidele saab rakendada foreach tsüklit
            //      suurem osa extensioneid annab ka vastuseks IEnumerable<T>
            //      objecti, kusjuures parameetri (vasakpoolne) T ei pruugi
            //      olla sama, mis tulemuse (parempoolne) T
            //      IEnumerable<TParam> -> Extension -> IEnumerable<TResult>

            //      extensionid töötavad niisiis hulga teisendajana, kus
            //      parameeter on sissetulev ja result on väljuv hulk
            //      punktidega üksteise järel 
            //      moodustavad nad siis nagu teisendus-toru

            //      ekstensionid jagunevad (jämedalt)
            //      * teisendajad - samapalju kui sisse, samapalju ka välja
            //          Select, OrderBy, OrderByDescending, Reverse ...
            //      * filtrid - osa sissetulevaid jäävad väljundisse minemata
            //          Where, Skip, Take, ...
            //      * agregaadid - sissetulnud koondatakse üheks tulemuseks
            //          Sum, Average, Count, First, Single, ElementAt ...
            //      * koondajad - sissetulnud koondatakse väiksemaks hulgaks
            //          GroupBy, ToLookup, ToDictionary, ...
            //      * laiendajad - sissetulnud hulkadest tekitatakse uus hulk
            //          Union, Zip, ...
            //      * hulgatüübi teisendajad
            //          ToArray, ToList, ...

            Console.WriteLine("\nedasi extensionite näited\n");

            // Alustame tüüpiliste filtritega

            int[] jada = { 3, 2, 1, 7, 8, 6, 4, 5, 9, 1, 6 ,};
            Inimene[] nimekiri =
            {
                new Inimene{Nimi = "Henn", Vanus = 63, Sugu = Sugu.Mees},
                new Inimene{Nimi = "Ants", Vanus = 40, Sugu = Sugu.Mees},
                new Inimene{Nimi = "Pille", Vanus = 22, Sugu = Sugu.Naine},
                new Inimene{Nimi = "Kalle", Vanus = 32, Sugu = Sugu.Mees},
                new Inimene{Nimi = "Malle", Vanus = 25, Sugu = Sugu.Naine},
                new Inimene{Nimi = "Ülle", Vanus = 28, Sugu = Sugu.Naine},
                new Inimene{Nimi = "Peeter", Vanus = 19, Sugu = Sugu.Mees},
                new Inimene{Nimi = "Anu", Vanus = 40, Sugu = Sugu.Naine},
                new Inimene{Nimi = "Juhan", Vanus = 37, Sugu = Sugu.Mees},
                new Inimene{Nimi = "Kalle", Vanus = 20, Sugu = Sugu.Mees},
            };

            Console.WriteLine(nimekiri.Count());

            var q1e = jada.Where(x => x % 2 == 0);
            var q1l = from x in jada where x % 2 == 0 select x;
            // mõlemad annavad tulemuseks jada, mis koosneb vaid paarisarvudest
            foreach(var x in q1e) Console.WriteLine(x);
            
            var q2e = jada.Skip(3); // 7,8,6,4,5,9,1,6
            var q3e = jada.Take(5); // 3,1,2,7,8

            var q4e = jada.Distinct(); // 7,8,6,4,5,9,1

            var q5e = nimekiri
                        .GroupBy(x => x.Nimi)
                        .Select(x => x.First());
            //Console.WriteLine("\nuniquenimekiri\n");
            //foreach (var x in q5e) Console.WriteLine(x);

            var q5l = from x in nimekiri
                      group x by x.Nimi into g
                      select g.First();
            Console.WriteLine("\nuniquenimekiri\n");
            foreach (var x in q5l) Console.WriteLine(x);

            var q6e = nimekiri.Where(x => x.Sugu == Sugu.Naine);
            var q6l = from x in nimekiri
                      where x.Sugu == Sugu.Naine
                      select x;
            // mõlemad annavad tulemuseks naiste nimekirja
            // foreach(var x in q6e) Console.WriteLine(x);

            // teisendusvõlur SELECT

            // select on väga universaalne teisendaja
            // samas väga lihtne - koosnedes ühest funktsioonist (lambda)
            // kus sissetuleva hulga element parameetriks ja resultaadiks
            // väljaminev hulk

            // koos selectiga on hea meenutada anonüümset structi

            var anonStruct = new { Nimi = "Henn", Vanus = 63 };
            //Console.WriteLine(anonStruct.Nimi); // trükib Henn
            //Console.WriteLine(anonStruct); // trükib { Nimi = "Henn", Vanus = 63 }
            // anonüümsel structil on nimelt oma ToString juba 'kõhus'

            // kui kasutada tupleid, siis lisandub veel lihtsam konstruktrsioon
            // (mine Package Manager Console aknasse ja ütle:
            //      install-package System.ValueTuple)

            var anonTuple = (Nimi: "Henn", Vanus : 63); // NB! : mitte =
            // Console.WriteLine(anonTuple.Nimi); // trükib Henn
            // Console.WriteLine(anonTuple); // trükib (Henn, 63)

            var sel1E = jada.Select(x => x * x);
            var sel1L = from x in jada select x * x;
            foreach (var x in sel1E) Console.WriteLine(x);
            // saame jada, mis koosneb esialgse jada ruutudest

            var sel2E = nimekiri
                .Where(x => x.Sugu == Sugu.Mees)
                .Select(x => $"mees nimega {x.Nimi} vanusega {x.Vanus}");

            var sel2L = from x in nimekiri
                        where x.Sugu == Sugu.Mees
                        select $"mees nimega {x.Nimi} vanusega {x.Vanus}";

            var sel3E = nimekiri
                .Select(x => new { x.Nimi, x.Vanus, Sünniaasta = DateTime.Now.Year - x.Vanus });

            var sel3L = from x in nimekiri
                        select new { x.Nimi, x.Vanus, Sünniaasta = DateTime.Now.Year - x.Vanus };

            //Console.WriteLine("\nnäitame selecti\n");
            //foreach (var x in sel3L) Console.WriteLine(x);

            var sel4E = jada
                .Where(x => x % 2 == 0)
                .Distinct()
                .Select(x => (Arv: x, Ruut: x * x));
            var sel4L = from g in
                            from x in jada
                            where x % 2 == 0
                            group x by x into g
                            select g.First()
                        select new { Arv = g, Ruut = g * g };
             foreach (var x in sel4L) Console.WriteLine(x);

            // mõlemad annavad paarid arvust ja tema ruudust
            // NB! LINQ avaldisel ei ole distincti
            // seepärast on siin kasutatud 'alampäringut'
            // taandrida on vaid loetavuse pärast (erinevalt näiteks pythonist)

            // edasi tüüpilisi agregaate

            var sum1e = jada.Where(x => x > 4).Sum();
            var sum1l = (from x in jada where x > 4 select x).Sum();
            // tulemuseks 7 + 8 + 6 + 5 + 9 + 6 = 41
            Console.WriteLine($"neljast suuremate arvude summa on {sum1e}");
            var avg1e = jada.Where(x => x > 4).DefaultIfEmpty().Average();
            Console.WriteLine($"nlejast suuremat arvude keskmine on {avg1e}"); // 6,8333333333
            // NB! keskmise arvutamisel tuleb hoolitseda, et ei oleks tühi hulk
            //      tühjal hulgal ei ole keskmist (saab vea)

            var avg2e = nimekiri
                        .Where(x => x.Sugu == Sugu.Mees)
                        .Average(x => x.Vanus);
            // või
            var avg2ea = nimekiri
                        .Where(x => x.Sugu == Sugu.Mees)
                        //.Where(x => x.Vanus > 100)
                        .Select(x => x.Vanus)
                        //.DefaultIfEmpty()
                        .Average();

            Console.WriteLine($"meeste keskmine vanus on {avg2ea}");

            var avg2l = (from x in nimekiri
                         where x.Sugu == Sugu.Mees
                         select x
                         ).Average(x => x.Vanus);
            var avg2la = (from x in nimekiri
                          where x.Sugu == Sugu.Mees
                          select x.Vanus)
                          .Average();
            // need kaks annavad samuti meeste keskmise vanuse

            var avg3e = nimekiri
                        .Where(x => x.Vanus > 100)
                        .DefaultIfEmpty()   // et vältida tühja hulga keskmist
                        .Average(x => x?.Vanus ?? 0); // et vältida null.Vanus exceptionit
            Console.WriteLine($"üle sajaaastaste keskmine vanus on {avg3e}");

            var avg4e = nimekiri
                        .Select(x => x.Vanus) // uus jada, mis koosneb vaid vanustest
                        .Where(x => x > 100)
                        .DefaultIfEmpty()
                        .Average();

            var avg4l = (from v in
                             from x in nimekiri
                             //where x.Vanus > 100
                             select x.Vanus
                         where v > 100
                         select v)
                         .DefaultIfEmpty()
                         .Average();

            var avg4la = (from x in nimekiri
                          where x.Vanus > 100
                          select x.Vanus)
                          .DefaultIfEmpty()
                          .Average();
            // natuke lihtsamalt sama asi   

            // funktsioonid First, Single, ElementAt annavad
            //      hulgast ühe elemendi
            //      First esimese (tühja hulga puhul viga)
            //      Single ainukese (kui rohkem või vähem, siis viga)
            //      ElementAt(n) n-inda (kui seda pole, siis viga)
            //      kõigil kolmel on paariline
            //      FirstOrDefault - tühja hulgaga ei saa viga
            //      SingleOrDefault - tühja hulgaga ei saa viga, kui aga rohkem kui 1 siis ikkagi viga
            //      ElementAtOrDefault(n) - ei anna viga 
            //      NB! arvude puhul on default 0, objektide puhul null

            //      === on hennu väljamõeldud "on sama asi kui"
            //      FirstOrDefault() === DefaultIfEmpty().First();
            //      ElementAt(n) === Skip(n-1).First()
            //      ElementAtOrDefault(n) === Skip(n-1).DefaultIfEmpty().First()

            var nimi1e = nimekiri
                    .Where(x => x.Vanus > 18)
                    .FirstOrDefault()?.Nimi??"pole sellist"; // küsimärk et vältida null.Nimi viga
            // Console.WriteLine($"esimene (ettejuhtuv) täisealine on {nimi1e}");

            var nimi2e = nimekiri
                    .Where( x => x.Sugu == Sugu.Naine)
                    .OrderBy(x => x.Vanus)
                    .FirstOrDefault()?.Nimi;
            // Console.WriteLine($"noorim naine on {nimi2e}");

            var nimi2l = (from x in nimekiri
                          where x.Sugu == Sugu.Naine
                          orderby x.Vanus
                          select x)
                          .FirstOrDefault()?.Nimi;
            // Console.WriteLine($"noorim naine on {nimi2l}");
            // mõlemad annava noorima naise nime

            var nimi3e = nimekiri
                    .OrderByDescending(x => x.Vanus)
                    .FirstOrDefault()?.Nimi;
            // Console.WriteLine($"vanim on {nimi3e}");

            var nimi3l = (from x in nimekiri
                          orderby x.Vanus descending
                          select x)
                          .FirstOrDefault()?.Nimi;
            // Console.WriteLine($"vanim on {nimi3l}");

            // nüüd rühmitamisest

            var g1e = jada.GroupBy(x => x % 2)
                    .OrderBy( x => x.Key)
                    .Select(x => new { x.Key, mitu = x.Count() })
                    .Select(x => $"{(x.Key == 0 ? "paarisarve" : "paarituid arve")} on {x.mitu}")
                    ;
            foreach (var x in g1e) Console.WriteLine(x);

            var g2l = from x in nimekiri
                      group x by x.Sugu into g
                      select new { Sugu = g.Key, Mitu = g.Count() };
            foreach(var x in g2l)
                Console.WriteLine($"{x.Sugu}-inimesi on {x.Mitu}");

            // group by tekitab IEnumerable asja, 
            // seda saab foreachiga läbi käia või 
            // mõne exensioniga 'jätkata'

            // group by kõrval on kaks teisendajat
            // ToDictionary ja ToLookup

            // dictionarile antakse ette kaks finktsiooni-lambdat
            // üks annab avaldise võtme (grupi) leidmiseks
            // teine andmete jaoks
            var dict1e = nimekiri
                    .Where(x => x.Nimi != "Kalle") // võtame Kalled välja muidu saame vea
                    .ToDictionary(x => x.Nimi, x => x);
            Console.WriteLine(dict1e["Henn"].Vanus);

            var lookup = nimekiri.ToLookup(x => x.Sugu);
            // nimekiri jaotatakse kaheks enumerabliks - mehed ja naised
            // edasi saab neid juba kasutada näiteks
            Console.Write("Meeste keskmine vanus on: ");
            Console.WriteLine(lookup[Sugu.Mees].Average(x => x.Vanus));
            Console.WriteLine("Ja kõige noorem tüdruk on");
            Console.WriteLine(lookup[Sugu.Naine].OrderBy(x => x.Vanus).First().Nimi);

            // NB! Torutehete hulk ei piirdu siintoodutega
            // neid tasub ikka ja jälle üle lugeda
        }
    }

    enum Sugu { Naine, Mees}
    class Inimene
    {
        public string Nimi;
        public int Vanus;
        public Sugu Sugu;
        public override string ToString() => $"{Sugu} {Nimi} vanusega {Vanus}";
    }
}
