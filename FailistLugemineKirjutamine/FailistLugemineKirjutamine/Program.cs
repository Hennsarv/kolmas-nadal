﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization; // seda meil läheb vaja - aga mitte ainult
using Newtonsoft.Json;

namespace FailistLugemineKirjutamine
{
    class Program
    {
        static void Main(string[] args)
        {
            string filenameTxt = @"..\..\Inimesed.txt";
            string filenameTxtout = @"..\..\InimesedOut.txt";
            string filenameXmlout = @"..\..\InimesedOut.xml";
            string filenameJsonout = @"..\..\InimesedOut.json";

            List<Inimene> inimesed1 = 
            File.ReadAllLines(filenameTxt)
                .Skip(1)
                .Select(x => x.Split(',').Select(y => y.Trim()).ToArray())
                .Select(x => new Inimene
                    {
                        Nimi = x[0],
                        Vanus = int.Parse(x[1])
                    })
                .ToList();

            inimesed1.Add(new Inimene { Nimi = "Malle", Vanus = 19 });
            inimesed1.Add(new Inimene { Nimi = "Ülle", Vanus = 19 });

            foreach (var x in inimesed1) Console.WriteLine(x);

            File.WriteAllLines(filenameTxtout,
                (new string[] { "Nimi,Vanus"}).Union(

                inimesed1
                .OrderBy(x => x.Nimi)
                .Select(x => $"{x.Nimi},{x.Vanus}"))
                
                );

            XmlSerializer xmlArr = new XmlSerializer(inimesed1.GetType());
            XmlSerializer xmlArr2 = new XmlSerializer(typeof(List<Inimene>));

            StringWriter sw = new StringWriter();

            xmlArr.Serialize(sw, inimesed1);
            File.WriteAllText(filenameXmlout, sw.ToString());

            // JSON - javascript object notation
            // Selleks vaja üht head tööriista
            // package manager console aknas
            // ütle install-package newtonsoft.json
            // using üles ...

            string json = JsonConvert.SerializeObject(inimesed1, Formatting.Indented);
            //Console.WriteLine(json);

            File.WriteAllText(filenameJsonout, json);

            // lihtne lugemine JSON failist
            string jsonIn = File.ReadAllText(filenameJsonout);
            List<Inimene> inimesed2 
                = JsonConvert.DeserializeObject<List<Inimene>>(jsonIn);
            Console.WriteLine("\nlugesime json failist\n");
            foreach (var x in inimesed2) Console.WriteLine(x);

            // lihtne lugemine XML failist
            string xmlIn = File.ReadAllText(filenameXmlout);
            List<Inimene> inimesed3 =
                (List<Inimene>)xmlArr.Deserialize(new StringReader(xmlIn));
            Console.WriteLine("\nlugesime xml failist\n");
            foreach (var x in inimesed3) Console.WriteLine(x);


        }
    }

    public class Inimene
    {
        public string Nimi { get; set; }
        public int Vanus { get; set; }

        public override string ToString()
            => $"{Nimi} vanus: {Vanus}";
    }
}
