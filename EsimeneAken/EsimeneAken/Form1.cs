﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EsimeneAken
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.label2.Text = "Ära siis virise";
        }

        private void button2_MouseHover(object sender, EventArgs e)
        {
            this.button2.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.label2.Text = "NO tõstame siis palka";
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string s =  this.textBox1.Text;
            try
            {
                int palk = int.Parse(s);
                this.label4.Text = $"aastas siis {palk * 12}";
            }
            catch
            {
                label4.Text = "";
            }
        }

        private void Numbrinupu_click(object sender, EventArgs e)
        {
            this.textBox1.Text += (sender as Button)?.Text??"";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Inimesed c = new Inimesed();
            c.ShowDialog();
        }
    }
}
