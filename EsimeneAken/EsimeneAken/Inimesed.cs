﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;

namespace EsimeneAken
{
    public partial class Inimesed : Form
    {
        List<Inimene> Nimekiri;
        static string filename = @"..\..\InimesedOut.json";
        static BindingSource Source = new BindingSource();

        public Inimesed()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Nimekiri = JsonConvert.DeserializeObject<List<Inimene>>(File.ReadAllText(filename));

        }

        private void Inimesed_Load(object sender, EventArgs e)
        {


            string filecontent = File.ReadAllText(filename);
            Nimekiri = JsonConvert.DeserializeObject<List<Inimene>>(filecontent);
            Source.DataSource = Nimekiri;
            Source.ListChanged += ListMuutus;
            Source.DataError += VigaAndmetes;
            #region Pooleli


            //  Binding firstNameBinding = new Binding("Text", personalDataBindingSource, "Firstname");
            Binding nimiBinding = new Binding("Nimi", Nimekiri, "Nimi");
            Binding vanusBinding = new Binding("Vanus", Nimekiri, "Vanus");
            nimiBinding.Parse += NimiParse;
            vanusBinding.Parse += VanusParse;

            #endregion

            this.dataGridView1.DataSource = Source;
            dataGridView1.Columns[0].HeaderText = "Eesnimi";
            dataGridView1.Columns[1].HeaderText = "Kui vana";
            //dataGridView1.Columns[1].Visible = false;
        }

        private void VigaAndmetes(object sender, BindingManagerDataErrorEventArgs e)
        {
            this.label3.Text = "Viga andmetes!";
        }

        private void VanusParse(object sender, ConvertEventArgs e)
        {
            if (e.DesiredType != typeof(int)) return;
            e.Value = ((int)(e.Value)).ToString();
        }

        private void NimiParse(object sender, ConvertEventArgs e)
        {
            
        }

        private void ListMuutus(object sender, ListChangedEventArgs e)
        {
            File.WriteAllText(filename, JsonConvert.SerializeObject(Nimekiri, Formatting.Indented));

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.dataGridView1.Columns[1].Visible = true;
        }

        private void tekstMuutus(object sender, EventArgs e)
        {
            Salvesta.Enabled =
                nimiBox.Text.Trim() != "" &&
                vanusBox.Text.Trim() != "" &&
                int.TryParse(vanusBox.Text.Trim(), out _) &&
                Nimekiri != null &&
                !Nimekiri.Select( x=>x.Nimi).Contains(nimiBox.Text.Trim())
                ;
        }

        private void Salvesta_Click(object sender, EventArgs e)
        {
            Inimene uus = new Inimene
            {
                Nimi = nimiBox.Text.Trim(),
                Vanus = int.Parse(vanusBox.Text.Trim())
            };

            Nimekiri.Add(uus);
            File.WriteAllText(filename, JsonConvert.SerializeObject(Nimekiri, Formatting.Indented));
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Source;
            dataGridView1.Refresh();

        }
    }
}
