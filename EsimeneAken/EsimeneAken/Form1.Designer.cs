﻿namespace EsimeneAken
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Nupp0 = new System.Windows.Forms.Button();
            this.Nupp2 = new System.Windows.Forms.Button();
            this.Nupp1 = new System.Windows.Forms.Button();
            this.Nupp4 = new System.Windows.Forms.Button();
            this.Nupp5 = new System.Windows.Forms.Button();
            this.Nupp3 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.Nupp7 = new System.Windows.Forms.Button();
            this.Nupp8 = new System.Windows.Forms.Button();
            this.Nupp6 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.Nupp9 = new System.Windows.Forms.Button();
            this.BSButton = new System.Windows.Forms.Button();
            this.CButton = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label1.Location = new System.Drawing.Point(163, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(384, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Kas sa oled oma palgaga rahul";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(166, 200);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Jah";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(300, 200);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Ei";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            this.button2.MouseHover += new System.EventHandler(this.button2_MouseHover);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(166, 292);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 3;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(169, 253);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 4;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(67, 253);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Palganumber";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(300, 259);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "label4";
            // 
            // Nupp0
            // 
            this.Nupp0.Location = new System.Drawing.Point(169, 302);
            this.Nupp0.Name = "Nupp0";
            this.Nupp0.Size = new System.Drawing.Size(27, 23);
            this.Nupp0.TabIndex = 7;
            this.Nupp0.Text = "0";
            this.Nupp0.UseVisualStyleBackColor = true;
            this.Nupp0.Click += new System.EventHandler(this.Numbrinupu_click);
            // 
            // Nupp2
            // 
            this.Nupp2.Location = new System.Drawing.Point(235, 302);
            this.Nupp2.Name = "Nupp2";
            this.Nupp2.Size = new System.Drawing.Size(27, 23);
            this.Nupp2.TabIndex = 8;
            this.Nupp2.Text = "2";
            this.Nupp2.UseVisualStyleBackColor = true;
            this.Nupp2.Click += new System.EventHandler(this.Numbrinupu_click);
            // 
            // Nupp1
            // 
            this.Nupp1.Location = new System.Drawing.Point(202, 302);
            this.Nupp1.Name = "Nupp1";
            this.Nupp1.Size = new System.Drawing.Size(27, 23);
            this.Nupp1.TabIndex = 9;
            this.Nupp1.Text = "1";
            this.Nupp1.UseVisualStyleBackColor = true;
            this.Nupp1.Click += new System.EventHandler(this.Numbrinupu_click);
            // 
            // Nupp4
            // 
            this.Nupp4.Location = new System.Drawing.Point(303, 302);
            this.Nupp4.Name = "Nupp4";
            this.Nupp4.Size = new System.Drawing.Size(27, 23);
            this.Nupp4.TabIndex = 13;
            this.Nupp4.Text = "4";
            this.Nupp4.UseVisualStyleBackColor = true;
            this.Nupp4.Click += new System.EventHandler(this.Numbrinupu_click);
            // 
            // Nupp5
            // 
            this.Nupp5.Location = new System.Drawing.Point(336, 302);
            this.Nupp5.Name = "Nupp5";
            this.Nupp5.Size = new System.Drawing.Size(27, 23);
            this.Nupp5.TabIndex = 12;
            this.Nupp5.Text = "5";
            this.Nupp5.UseVisualStyleBackColor = true;
            this.Nupp5.Click += new System.EventHandler(this.Numbrinupu_click);
            // 
            // Nupp3
            // 
            this.Nupp3.Location = new System.Drawing.Point(270, 302);
            this.Nupp3.Name = "Nupp3";
            this.Nupp3.Size = new System.Drawing.Size(27, 23);
            this.Nupp3.TabIndex = 11;
            this.Nupp3.Text = "3";
            this.Nupp3.UseVisualStyleBackColor = true;
            this.Nupp3.Click += new System.EventHandler(this.Numbrinupu_click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(267, 292);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 10;
            // 
            // Nupp7
            // 
            this.Nupp7.Location = new System.Drawing.Point(404, 302);
            this.Nupp7.Name = "Nupp7";
            this.Nupp7.Size = new System.Drawing.Size(27, 23);
            this.Nupp7.TabIndex = 17;
            this.Nupp7.Text = "7";
            this.Nupp7.UseVisualStyleBackColor = true;
            this.Nupp7.Click += new System.EventHandler(this.Numbrinupu_click);
            // 
            // Nupp8
            // 
            this.Nupp8.Location = new System.Drawing.Point(437, 302);
            this.Nupp8.Name = "Nupp8";
            this.Nupp8.Size = new System.Drawing.Size(27, 23);
            this.Nupp8.TabIndex = 16;
            this.Nupp8.Text = "8";
            this.Nupp8.UseVisualStyleBackColor = true;
            this.Nupp8.Click += new System.EventHandler(this.Numbrinupu_click);
            // 
            // Nupp6
            // 
            this.Nupp6.Location = new System.Drawing.Point(371, 302);
            this.Nupp6.Name = "Nupp6";
            this.Nupp6.Size = new System.Drawing.Size(27, 23);
            this.Nupp6.TabIndex = 15;
            this.Nupp6.Text = "6";
            this.Nupp6.UseVisualStyleBackColor = true;
            this.Nupp6.Click += new System.EventHandler(this.Numbrinupu_click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(368, 292);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 14;
            // 
            // Nupp9
            // 
            this.Nupp9.Location = new System.Drawing.Point(470, 302);
            this.Nupp9.Name = "Nupp9";
            this.Nupp9.Size = new System.Drawing.Size(27, 23);
            this.Nupp9.TabIndex = 18;
            this.Nupp9.Text = "9";
            this.Nupp9.UseVisualStyleBackColor = true;
            this.Nupp9.Click += new System.EventHandler(this.Numbrinupu_click);
            // 
            // BSButton
            // 
            this.BSButton.Location = new System.Drawing.Point(169, 343);
            this.BSButton.Name = "BSButton";
            this.BSButton.Size = new System.Drawing.Size(27, 23);
            this.BSButton.TabIndex = 19;
            this.BSButton.Text = "BS";
            this.BSButton.UseVisualStyleBackColor = true;
            // 
            // CButton
            // 
            this.CButton.Location = new System.Drawing.Point(202, 343);
            this.CButton.Name = "CButton";
            this.CButton.Size = new System.Drawing.Size(27, 23);
            this.CButton.TabIndex = 19;
            this.CButton.Text = "C";
            this.CButton.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(473, 26);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(83, 30);
            this.button3.TabIndex = 20;
            this.button3.Text = "Calc";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(591, 450);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.CButton);
            this.Controls.Add(this.BSButton);
            this.Controls.Add(this.Nupp9);
            this.Controls.Add(this.Nupp7);
            this.Controls.Add(this.Nupp8);
            this.Controls.Add(this.Nupp6);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Nupp4);
            this.Controls.Add(this.Nupp5);
            this.Controls.Add(this.Nupp3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Nupp1);
            this.Controls.Add(this.Nupp2);
            this.Controls.Add(this.Nupp0);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Nupp0;
        private System.Windows.Forms.Button Nupp2;
        private System.Windows.Forms.Button Nupp1;
        private System.Windows.Forms.Button Nupp4;
        private System.Windows.Forms.Button Nupp5;
        private System.Windows.Forms.Button Nupp3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Nupp7;
        private System.Windows.Forms.Button Nupp8;
        private System.Windows.Forms.Button Nupp6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button Nupp9;
        private System.Windows.Forms.Button BSButton;
        private System.Windows.Forms.Button CButton;
        private System.Windows.Forms.Button button3;
    }
}

