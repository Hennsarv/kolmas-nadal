﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EsimeneAken
{
    public partial class Calc : Form
    {
        public Calc()
        {
            InitializeComponent();
        }

        private void Liida_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            Arvuta("+");
        }


        void Arvuta(string tehe)
        {
            // liida
            try
            {
                int arv1 = int.Parse(this.textBox1.Text);
                int arv2 = int.Parse(this.textBox2.Text);
                // int tulemus = f (arv1, arv2);
                int tulemus = 0;
                switch (tehe)
                {
                    case "+": tulemus = arv1 + arv2; break;
                    case "-": tulemus = arv1 - arv2; break;
                    case "*": tulemus = arv1 * arv2; break;
                    case "/": tulemus = arv1 / arv2; break;
                }
                label1.Text = tulemus.ToString();

            }
            catch { label1.Text = "ei arvuta"; }
        }

        private void Korruta_Click(object sender, EventArgs e)
        {
            Arvuta("*");
        }

        private void Lahuta_Click(object sender, EventArgs e)
        {
            Arvuta("-");
        }

        private void Jaga_Click(object sender, EventArgs e)
        {
            Arvuta("/");
        }

        private void Number_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            string number = b.Text;
            (t ?? textBox1).Text += number;
        }

        

        private void textbox_Click(object sender, EventArgs e)
        {
            t = (TextBox)sender;
            foreach (Control c in this.Controls)
                if (c is TextBox tx) tx.BackColor = Color.White; 
            t.BackColor = Color.Yellow;
        }

        private TextBox t = null;
    }
}
